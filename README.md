# Objective:
#### To develop a website that is mobile responsive and allows visitors to message/email the site owner. In addition, the visitor gets a confirmation if their message has been sent or if there was an error that occurred.

#Libraries used:
* HTML
* CSS
* Bootstrap 4
* Google Fonts
* Font Awesome
* JavaScript
* PHP

With the combination of **HTML**, **CSS** and **Bootstrap 4**, I was able to design the mobile-responsive layout. I used **Google Fonts** for the *Freestyle* logo, header text and *About* intro. Using **Font Awesome**’s vast library of icons, I applied their social media icons to the *Stalk Me* section.

Adding the task of visitors being able to message the site owner was going to be the biggest challenge for me. While being able to use PHP for server-side validation, I wanted to somehow display to the user that their message had been successfully sent (or not) within the same region as the message form. In previous projects, I had redirected the user to a *thankyou.html* page when their message had been sent; within this *thankyou* page, I had a button that links them back to *index.html*. While functional, it wasn’t the greatest UX.

![Empty Form](screenshots/empty-form.png)

![Filled Form](screenshots/filled-form.png)

Below is the JS code written for three responsibilities. The first responsibility is letting the visitor know - after they’ve hit the *Submit* button - that their message is being processed to be sent by transforming the button text to say *Sending*. Meanwhile, JS passes on the validation over to the PHP file where it conforms that there is a name with 50 characters or less, a valid email address that confirms to the email structure standard and a message with a maximum of 6000 characters. Once all three validations are satisfied, the message is sent in email format to the email address set, preferably the email address belonging to the site owner. 

![JS to PHP handler](screenshots/js-to-php.png)

![PHP Validation](screenshots/php-validation.png)

Once PHP responds to JS announcing, “*good to go!*”, JS changes the form text using hidden HTML elements within the form to either **Success! Your message has been sent** or if an error occurred: **Error, sorry there was an error sending your message**.

![JS Results As Success!](screenshots/sent-form-success.png)

![Email Received!](screenshots/email-received.png)

# Lessons learned:
I am not sure why, but I ran into issues with the mobile rendering, but this experience has persuaded me to start using the *mobile-first* approach. In addition, I need be attentive to the iPad renderings; while it seems that the desktop version works on it, some elements are off-centered or text is too big for their respective container. 
